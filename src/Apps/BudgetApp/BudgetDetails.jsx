import { IonButton, IonItem, IonLabel, IonList,IonNote } from "@ionic/react";
import React from "react";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import { actionBudgetNavTo } from "../../redux/budgetReducer";
const AddIncomeExpens = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
  align-items: center;
  margin-top: 0.8rem;
  padding-bottom: 0.5rem;
  border-bottom-style: ridge;
`;

const HeaderTotalStyle = styled.div`
  border-style: solid;
  border-width: thin;
  border-color: #aaa;
`;

const BudgetDetails = () => {
  const dispatch = useDispatch(null);
  const selectedBudgetIndex = useSelector(
    (state) => state.budgetReducer.selectedBudgetIndex
  );
  const totalBudget = useSelector(
    (state) => state.budgetReducer.totalBudgetArr[selectedBudgetIndex]
  );
  const budgetData = useSelector(
    (state) => state.budgetReducer.budgetDataArr[selectedBudgetIndex]
  );
  return (
    <>
      <AddIncomeExpens>
        <IonButton
          className="shadow-2 br2"
          color="success"
          onClick={() => dispatch(actionBudgetNavTo("AddIncomeExpens/Income"))}
        >
          Add Income
        </IonButton>
        <IonButton
          className="shadow-2 br2"
          color="danger"
          onClick={() => dispatch(actionBudgetNavTo("AddIncomeExpens/Expens"))}
        >
          Add Income
        </IonButton>
        <HeaderTotalStyle className="br2 shadow-2 pa2">
          <IonLabel>Total : </IonLabel>
          <IonLabel color={totalBudget > 0 ? "success" : "danger"}>
            {totalBudget}
          </IonLabel>
        </HeaderTotalStyle>
      </AddIncomeExpens>
      <IonList>
        {budgetData.map((item) => {
          return (
            <IonItem lines="full">
            <IonNote slot="start">{item.date}</IonNote>
              <IonLabel slot="start">{item.label}</IonLabel>
              <IonLabel
                slot="end"
                color={item.type === "Income" ? "success" : "danger"}
              >
                {item.amount}
              </IonLabel>
            </IonItem>
          );
        })}
      </IonList>
    </>
  );
};

export default BudgetDetails;
