import { IonInput, IonItem, IonLabel, IonList, IonButton } from "@ionic/react";
import React, { useState } from "react";
import dayjs from "dayjs";
import { DatePicker } from "antd";
import { useDispatch } from "react-redux";
import {
  actionAddIncomeExpens,
  actionBudgetGoBack,
} from "../../redux/budgetReducer";
const AddBudgetIncomeExpens = ({ type }) => {
  const dispatch = useDispatch(null);
  const [currentDate, setCurrentDate] = useState(dayjs());
  const handleSubmit = (e) => {
    e.preventDefault();
    let date = "";
    if (e.target[2].value.Date === undefined) {
      date = dayjs(currentDate).format("YYYY-MM-DD");
    } else {
      date = dayjs(e.target[2].value.Date).format("YYYY-MM-DD");
    }
    dispatch(
      actionAddIncomeExpens({
        type: type,
        label: e.target[0].value,
        amount: e.target[1].value,
        date: date,
      })
    );
    dispatch(actionBudgetGoBack());
  };
  return (
    <form onSubmit={handleSubmit}>
      <IonList>
        <IonItem>
          <IonLabel>Label :</IonLabel>
          <IonInput type="text" required />
        </IonItem>
        <IonItem>
          <IonLabel>Amount :</IonLabel>
          <IonInput required type="number" step="0.01" />
        </IonItem>
        <IonItem>
          <IonLabel>Date :</IonLabel>
          <DatePicker
            defaultValue={currentDate}
            onChange={(date) => setCurrentDate(date)}
          />
        </IonItem>
        <div className="tc pl4 pr4 pt2">
          <IonButton type="submit" expand="block">
            Add
          </IonButton>
        </div>
      </IonList>
    </form>
  );
};

export default AddBudgetIncomeExpens;
