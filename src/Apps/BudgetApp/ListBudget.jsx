import { IonItem, IonLabel, IonList } from "@ionic/react";
import React from "react";
import { useSelector,useDispatch } from "react-redux";
import { actionBudgetNavTo, actionOpenBudget } from "../../redux/budgetReducer";
const ListBudget = () => {
  const dispatch = useDispatch(null);
  const budgetsArr = useSelector((state) => state.budgetReducer.budgetsArr);
  const totalBudgetArr = useSelector(
    (state) => state.budgetReducer.totalBudgetArr
  );
  return (
    <>
      <IonList>
        {budgetsArr &&
          budgetsArr.map((label, index) => {
            return (
              <IonItem lines="full" button onClick={() => {
                dispatch(actionOpenBudget(index))
                dispatch(actionBudgetNavTo(`Budget Details/${label}`))
              }}>
                <IonLabel>{label}</IonLabel>
                <IonLabel slot="end" color={totalBudgetArr[index] > 0 ? "success" : "danger"}>{totalBudgetArr[index]}</IonLabel>
              </IonItem>
            );
          })}
      </IonList>
    </>
  );
};

export default ListBudget;
