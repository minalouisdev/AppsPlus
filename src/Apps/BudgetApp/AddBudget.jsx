import { IonButton, IonInput, IonItem, IonLabel, IonList } from '@ionic/react'
import React,{useState} from 'react'
import dayjs from 'dayjs'
import {DatePicker} from 'antd';
import { useDispatch } from 'react-redux';
import { actionAddBudget, actionBudgetGoBack } from '../../redux/budgetReducer';
const AddBudget = () => {
    const dispatch = useDispatch(null);
    const [currentDate,setCurrentDate] = useState(dayjs());
    const handleSubmit = (e) => {
        e.preventDefault();
        let date = '';
        if(e.target[2].value.Date === undefined){
            date = dayjs(currentDate).format("YYYY-MM-DD");
        } else {
            date = dayjs(e.target[2].value.Date).format("YYYY-MM-DD");
        }
        dispatch(actionAddBudget({
            label : e.target[0].value,
            amount : e.target[1].value,
            date : date
        }))
        dispatch(actionBudgetGoBack())
    }
  return (
    <>
     <IonList>
     <form onSubmit={handleSubmit}>
     <IonItem>
            <IonLabel>Label :</IonLabel>
            <IonInput 
                required
                type='text'
            />
        </IonItem>
        <IonItem>
            <IonLabel>Amount :</IonLabel>
            <IonInput 
                required
                step='0.01'
                type='number'
            />
        </IonItem>
        <IonItem>
            <IonLabel>Date :</IonLabel>
            <DatePicker 
                defaultValue={currentDate}
                onChange={(date) => setCurrentDate(date)}
            />
        </IonItem>
        <div className='tc pl4 pr4 pt2'>
            <IonButton type='submit' expand='block'>Add</IonButton>
        </div>
     </form>
     </IonList> 
    </>
  )
}

export default AddBudget
