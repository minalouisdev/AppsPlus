import React from 'react'
import {isMobile} from 'react-device-detect';
import { useHistory } from 'react-router';
const MainPage = () => {
    const history = useHistory(null);
  return (
    <>
     {isMobile ? history.push('/Mobile/MainPage') : <p>desktop under dev</p> } 
    </>
  )
}

export default MainPage
