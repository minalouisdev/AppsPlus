import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import AddBudget from "../../Apps/BudgetApp/AddBudget";
import ListBudget from "../../Apps/BudgetApp/ListBudget";
import BudgetDetails from "../../Apps/BudgetApp/BudgetDetails";
import AddBudgetIncomeExpens from "../../Apps/BudgetApp/AddBudgetIncomeExpens";
import { useDispatch, useSelector } from "react-redux";
import {
  actionBudgetGoBack,
  actionBudgetNavTo,
} from "../../redux/budgetReducer";

const BudgetPage = () => {
  const dispatch = useDispatch(null);
  const budgetAppNavArr = useSelector(
    (state) => state.budgetReducer.budgetAppNavArr
  );
  const TitleCloseBtn = () => {
    return (
      <IonButtons slot="end">
        <IonButton
          color="danger"
          onClick={() => dispatch(actionBudgetGoBack())}
        >
          Close
        </IonButton>
      </IonButtons>
    );
  };

  const handleView = () => {
    let content =
      budgetAppNavArr.length === 0
        ? "default/0"
        : budgetAppNavArr[budgetAppNavArr.length - 1].split("/");
    switch (content[0]) {
      case "Add Budget":
        return <AddBudget />;
      case "Budget Details":
        return <BudgetDetails />;
      case "AddIncomeExpens":
        return <AddBudgetIncomeExpens type={content[1]} />;
      default:
        return <ListBudget />;
    }
  };
  const handleHeader = () => {
    let content =
      budgetAppNavArr.length === 0
        ? "default/0"
        : budgetAppNavArr[budgetAppNavArr.length - 1].split("/");
    switch (content[0]) {
      case "Add Budget":
        return (
          <>
            <IonTitle>Add Budget</IonTitle>
            <TitleCloseBtn />
          </>
        );
      case "Budget Details":
        return (
          <>
            <IonTitle>{content[1]}</IonTitle>
            <TitleCloseBtn />
          </>
        );
      case "AddIncomeExpens":
        return (
          <>
            <IonTitle>Add {content[1]}</IonTitle>
            <TitleCloseBtn />
          </>
        );
      default:
        return (
          <>
            <IonBackButton slot="start" />
            <IonTitle>Budget</IonTitle>
            <IonButtons slot="end">
              <IonButton
                onClick={() => dispatch(actionBudgetNavTo("Add Budget"))}
              >
                Add Budget
              </IonButton>
            </IonButtons>
          </>
        );
    }
  };
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar mode="ios">{handleHeader()}</IonToolbar>
      </IonHeader>
      <IonContent fullscreen>{handleView()}</IonContent>
    </IonPage>
  );
};

export default BudgetPage;
