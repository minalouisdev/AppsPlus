import { IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react'
import React from 'react'
import MainMobileMenu from '../../components/MainMobileMenu'
const MainMobilePage = () => {
  return (
    <IonPage>
        <MainMobileMenu />
        <IonHeader>
            <IonToolbar mode='ios'>
                <IonMenuButton slot='start'/>
                <IonTitle>
                    AppsPlus
                </IonTitle>
            </IonToolbar>
        </IonHeader>
        <IonContent fullscreen id='MainMobilePage'>
        </IonContent>
    </IonPage>
  )
}

export default MainMobilePage
