// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDNjYjSF8Nj9-ewmrivivmNvJM4P22-p8I",

    authDomain: "appsplus-ef1c3.firebaseapp.com",
  
    projectId: "appsplus-ef1c3",
  
    storageBucket: "appsplus-ef1c3.appspot.com",
  
    messagingSenderId: "394937477958",
  
    appId: "1:394937477958:web:c2f20163c6ccf44d711347",
  
    measurementId: "G-ZJ25MS02F0"
  
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);