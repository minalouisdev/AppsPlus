import { combineReducers } from "redux";
import storage from 'redux-persist/lib/storage';
import { persistReducer } from "redux-persist";
import { configureStore } from "@reduxjs/toolkit";
import budgetReducer from "./budgetReducer";
const reducers = combineReducers({
   budgetReducer,
})

const persistConfig = {
    key : "root",
    storage,
    whitelist : ['budgetReducer']
}

const persistedReducer = persistReducer(persistConfig,reducers);

export const store = configureStore({
    reducer : persistedReducer,
    devTools : true
})