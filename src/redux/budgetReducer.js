import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  budgetsArr: [],
  totalBudgetArr: [],
  budgetDataArr: [],
  budgetAppNavArr: [],
  selectedBudgetIndex: null,
};

/**
 * budgetDataArr : [
 *   [
 *      {
 *          date : YYYY-MM-DD
 *          type : Income , Expens
 *          amount : number
 *          label : string
 *      }
 *  ]
 * ]
 */

const budgetReducer = createSlice({
  name: "budgetReducer",
  initialState,
  reducers: {
    actionBudgetNavTo: (state, action) => {
      state.budgetAppNavArr.push(action.payload);
    },
    actionBudgetGoBack: (state, action) => {
      state.budgetAppNavArr.pop();
    },
    actionAddBudget: (state, action) => {
      /**
       * payload : {
       *  label : string
       *  date : string,
       *  amount : number
       * }
       */
      state.budgetsArr.push(action.payload.label);
      state.totalBudgetArr.push(action.payload.amount);
      state.budgetDataArr.push([
        {
          label: "Start Amount",
          date: action.payload.date,
          type: "Income",
          amount: action.payload.amount,
        },
      ]);
    },
    actionOpenBudget: (state, action) => {
      /** payload budget index */
      state.selectedBudgetIndex = action.payload;
    },
    actionAddIncomeExpens: (state, action) => {
      /**
       * payload : {
       *  type : string Income or Expens
       *  label : string
       *  amount : number
       *  date : YYYY-MM-DD
       * }
       */
      state.budgetDataArr[state.selectedBudgetIndex].push({
        label: action.payload.label,
        type: action.payload.type,
        amount: action.payload.amount,
        date: action.payload.date,
      });
      if (action.payload.type === "Income") {
        state.totalBudgetArr[state.selectedBudgetIndex] =
          parseFloat(state.totalBudgetArr[state.selectedBudgetIndex]) +
          parseFloat(action.payload.amount);
      } else if (action.payload.type === "Expens") {
        state.totalBudgetArr[state.selectedBudgetIndex] =
          parseFloat(state.totalBudgetArr[state.selectedBudgetIndex]) -
          parseFloat(action.payload.amount);
      }
    },
  },
});

export const {
  actionBudgetNavTo,
  actionBudgetGoBack,
  actionAddBudget,
  actionOpenBudget,
  actionAddIncomeExpens
} = budgetReducer.actions;
export default budgetReducer.reducer;
